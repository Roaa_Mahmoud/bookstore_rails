class Order < ActiveRecord::Base
  has_many :orderSpecs
  belongs_to :user
  has_many :books, through: :orderSpecs
end
