class Book < ActiveRecord::Base
  belongs_to :category
  has_many :orderSpecs
  has_many :cart_items
 has_many :carts, :through => :cart_items

end
