class CategoriesController < ApplicationController
  def create
    @book = Book.find(params[:book_id])
    @category = @book.categories.create(categories_params)
    redirect_to article_path(@article)
  end
  private
    def categories_params
      params.require(:category).permit(:name)
    end
end
