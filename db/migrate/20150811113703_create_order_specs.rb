class CreateOrderSpecs < ActiveRecord::Migration
  def change
    create_table :order_specs do |t|
      t.integer :quantity
      t.timestamps null: false
    end
  end
end
