class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :user_name
      t.string :user_address
      t.boolean :is_admin
      t.string :password
      t.string :email
      validates :password, length: { in: 6..10 }, presence: true
      t.timestamps null: false
    end
  end
end
