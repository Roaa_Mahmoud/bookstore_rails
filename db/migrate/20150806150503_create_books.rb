class CreateBooks < ActiveRecord::Migration
  def change
    create_table :books do |t|
      t.integer :available_quantity
      t.string :name
      t.integer :price
      validates :name, presence: true
      validates :available_quantity, numercality: { greater_than_or_equal_to: 0 }
      t.timestamps null: false
    end
  end
end
